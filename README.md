# React Crypto Market Dashboard Project.
<br>


## Table of contents
   - [1. Description](#1-description)
   - [2. Initial Design](#2-design)
   - [3. Prototype](#3-prototype)
   - [4. Project information](#4-project-information)
   - [5. Setup](#5-setup)
   - [6. Project Structure](#6-project-structure)
   - [7. Usage](#7-usage)
   - [8. Documentation](#8-documentation)


<br>


### **1. Description**
<br> 


Application with user authentication where user can explore crypto market dashboard that visualizes a predefined set of crypto market indexes from 3rd party API.
- deployed to <a href="https://deshka.gitlab.io/crypto-dashboard" target="_blank">Gitlab page</a>
<br>

### **2. Initial Design**
<br>

 Initial design is created with figma - [Figma Design](https://www.figma.com/file/sHykSXnAmYuFATmZnSSmN7/StockMarket?node-id=1%3A6)

<br>
<br>

### **3. Prototype**
<br>

 Prototype is created with figma  - [Figma Prototype](https://www.figma.com/proto/sHykSXnAmYuFATmZnSSmN7/StockMarket?node-id=6%3A22&scaling=scale-down&page-id=1%3A6&starting-point-node-id=6%3A22)

<br>
<br>

### **4. Project information**
<br>

- Language and version: **JavaScript ES2020**
- Platform and version: **Node 14.0+**
- Core Packages:  **React**,  **Redux**, **Webpack** , **Babel**, **ESLint**,**json server**
- Libraries: **Styled-Components**, **axios**, **Jest**, **React Testing Library**, **husky**

<br>
<br>

### **5. Setup**
   - 1. Clone/download repo
   - 2. yarn install (or npm install for npm) a-for client(src folder) and server

<br>
<br>

### **6. Project Structure**
- Project structure consists of two main parts - back-end (folder server) and front-end (folder client)
      
   **Front-end - folder (client)**
     - `src/App.js` - the entry point of the frontend project
     - `src/components` - contains shared components
     - `src/pages` - contains pages corresponding to routes specified in the App.js file
     - `src/redux` - contains redux settings- actions , reducers , types 
     - `src/utils` - contains reusable functions
     - `src/config` - contains api configuration files

   <br>

   **Back-end - folder (server)**
   - `server` - the entry point of the server part. Json server is used to fake server part

<br>
<br>

### **7. Usage**
   - `npm run start` - starts the project -App served @ http://localhost:3000//crypto-dashboard/

All commands:

- `npm run start` - starts the project
- `npm run lint` - run lint
- `npm run lint:fix` - fix lint errors
- `npm run format` - Format code
- `npm run build `- create bundle
- `npm run start` -run server (user server folder)
- `npm run test` -run unit and integration tests
- `npm run test:coverage` -run test coverage
<br>
<br>


###  **8. Documentation**
   <br>

   **Public routes**
   - `Route - crypto-dashboard/` - directs to `Login` page
   - `Route - crypto-dashboard/register` - directs to `Register` page

   <br>

   **Private routes**
   - `Route - crypto-dashboard/dashboard` - directs to `Dashboard` page,only if the user is logged in
   - `Route - crypto-dashboard/coins/:id` - directs to `CoinPage` page and render  detailed information for selected crypto currency 
   - `Route - crypto-dashboard/profile` - directs to `Profile` page and render  information about current user


<br>
<br>


**Login View**

![Login](./images/Login.png)


<br>
<br>


<br>
<br>


**Register View**

![Register](./images/Register.png)


<br>
<br>

<br>
<br>


**Dashboard View**

![Dashboard](./images/Dashboard.png)


<br>
<br>

<br>
<br>


**CoinPage View**

![CoinPage](./images/CoinPage.png)


<br>
<br>

<br>
<br>


**Profile View**

![Profile](./images/Profile.png)


<br>
<br>