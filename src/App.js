import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { ProtectedRoute } from './components';

import { Login, NotFound, Register, Profile, Dashboard, CoinPage } from './pages';

const App = () => {
  return (
    <BrowserRouter basename="/crypto-dashboard">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="register" element={<Register />} />
        <Route path="dashboard" element={<ProtectedRoute component={Dashboard} />} />
        <Route path="profile" element={<ProtectedRoute component={Profile} />} />
        <Route path="coins/:id" element={<ProtectedRoute component={CoinPage} />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
