import { SET_DOLLAR, SET_EURO } from './currencyTypes';

export const setEURO = () => {
  return {
    type: SET_EURO
  };
};

export const setDollar = () => {
  return {
    type: SET_DOLLAR
  };
};
