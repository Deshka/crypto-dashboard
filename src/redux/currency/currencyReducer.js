import { SET_EURO, SET_DOLLAR } from './currencyTypes';

const initialState = {
  currency: 'EUR',
  symbol: '€'
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_EURO:
      return {
        ...state,
        currency: 'EUR',
        symbol: '€'
      };
    case SET_DOLLAR:
      return {
        ...state,
        currency: 'USD',
        symbol: '$'
      };
    default:
      return state;
  }
};

export default reducer;
