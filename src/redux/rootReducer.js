import { combineReducers } from 'redux';

import authReducer from './auth/authReducer';
import modalReducer from './modal/modalReducer';
import currencyReducer from './currency/currencyReducer';
const rootReducer = combineReducers({
  auth: authReducer,
  modal: modalReducer,
  currency: currencyReducer
});

export default rootReducer;
