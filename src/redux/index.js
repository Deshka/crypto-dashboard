import store from './store';
import rootReducer from './rootReducer';
import { fetchUser, createUser, updateUser } from './auth/authActions';
import { hideModal, showModal } from './modal/modalActions';
import { setEURO, setDollar } from './currency/currencyActions';
export {
  store,
  fetchUser,
  createUser,
  updateUser,
  hideModal,
  showModal,
  setEURO,
  setDollar,
  rootReducer
};
