import React from 'react';
import { Link } from 'react-router-dom';
import {
  Center,
  Container,
  Left,
  MenuItem,
  OptionItems,
  Right,
  Select,
  StyledIconHome,
  StyledIconLogout,
  StyledIconUser,
  Wrapper
} from './Navbar.styles';
import { useDispatch, useSelector } from 'react-redux';
import { logoutUser } from '../../redux/auth/authActions';
import { setEURO, setDollar } from '../../redux';

const Navbar = () => {
  const currency = useSelector((state) => state.currency);
  const dispatch = useDispatch();
  const handleLogout = () => dispatch(logoutUser());

  const handleSetEuro = () => dispatch(setEURO());
  const handleSetDollar = () => dispatch(setDollar());

  const handleSelect = (e) => {
    e.target.value === 'EUR' ? handleSetEuro() : handleSetDollar();
  };
  return (
    <Container>
      <Wrapper>
        <Left>
          <Link to="/dashboard">
            <MenuItem>
              <StyledIconHome />
            </MenuItem>
          </Link>
          <Link to="/profile">
            <MenuItem>
              <StyledIconUser />
            </MenuItem>
          </Link>
        </Left>
        <Center>Crypto Hunter</Center>
        <Right>
          <Select onClick={(e) => handleSelect(e)}>
            <OptionItems value={'EUR'}>EUR</OptionItems>
            <OptionItems value={'USD'}>USD</OptionItems>
          </Select>
          <MenuItem>
            <StyledIconLogout onClick={handleLogout} />
          </MenuItem>
        </Right>
      </Wrapper>
    </Container>
  );
};
export default Navbar;
