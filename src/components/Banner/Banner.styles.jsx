import styled from 'styled-components';
export const StyledDashboardContainer = styled.div`
  background-color: darkblue;
  width: 100vw;
  margin-left: calc(50% - 50vw);
`;
export const StyledWrapperBanner = styled.div`
  background-image: url(../banner2.jpg);
`;
export const StyledContainerBanner = styled.div`
  height: 400px;
  display: flex;
  flex-direction: column;
  padding-top: 25px;
  justify-content: space-around;
`;
export const StyledTagline = styled.div`
  display: flex;
  height: 40%;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;

export const StyledBannerContent = styled.h1`
  color: darkgrey;
  text-transform: capitalize;
`;
