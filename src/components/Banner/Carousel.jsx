import axios from 'axios';
import React, { useEffect, useState } from 'react';

import AliceCarousel from 'react-alice-carousel';
import { useSelector } from 'react-redux';

import { trendingCoins } from '../../config';
import { numberWithCommas } from '../../utils';
import {
  StyledDiv,
  StyledImg,
  StyledLink,
  StyledSpanContainer
} from './Carousel.styles';
const CarouselView = () => {
  const currency = useSelector((state) => state.currency);

  const [trending, setTrending] = useState([]);

  const fetchTrendingCoins = async () => {
    const { data } = await axios.get(trendingCoins(currency.currency));

    setTrending(data);
  };

  useEffect(() => {
    fetchTrendingCoins();
  }, [currency.currency]);

  const items = trending.map((coin) => {
    let profit = coin?.price_change_percentage_24h >= 0;
    return (
      <StyledLink to={`/coins/${coin.id}`} key={coin.id}>
        <StyledImg src={coin?.image} alt={coin.name} />
        <StyledSpanContainer>
          {coin?.symbol}
          &nbsp;
          <StyledSpanContainer
            style={{
              color: profit > 0 ? 'rgb(14, 203, 129)' : 'red',
              fontWeight: 500
            }}
          >
            {profit && '+'}
            {coin?.price_change_percentage_24h?.toFixed(2)}%
          </StyledSpanContainer>
        </StyledSpanContainer>
        <StyledSpanContainer style={{ fontSize: 22, fontWeight: 500 }}>
          {currency.symbol} {numberWithCommas(coin?.current_price.toFixed(2))}
        </StyledSpanContainer>
      </StyledLink>
    );
  });

  const responsive = {
    0: {
      items: 2
    },
    512: {
      items: 4
    }
  };
  return (
    <StyledDiv>
      <AliceCarousel
        mouseTracking
        infinite
        autoPlayInterval={1000}
        animationDuration={1500}
        disableDotsControls
        disableButtonsControls
        responsive={responsive}
        autoPlay
        items={items}
      />
    </StyledDiv>
  );
};
export default CarouselView;
