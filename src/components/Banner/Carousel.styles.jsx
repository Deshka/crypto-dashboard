/* eslint-disable no-undef */
import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const StyledLink = styled(Link)`
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
  color: white;
  text-transform: uppercase;
`;
export const StyledImg = styled.img`
  src: ${(props) => props.image};
  alt: ${(props) => props.name};
  height: 80px;
  margin-bottom: 10px;
`;
export const StyledSpanContainer = styled.span``;

export const StyledDiv = styled.div`
  height: 50%;
  display: flex;
  align-items: center;
`;
