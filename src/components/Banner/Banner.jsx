import React from 'react';
import { CarouselView } from '..';
import {
  StyledWrapperBanner,
  StyledContainerBanner,
  StyledTagline,
  StyledBannerContent
} from './Banner.styles';

const Banner = () => {
  return (
    <StyledWrapperBanner>
      <StyledContainerBanner>
        <StyledTagline>
          <StyledBannerContent>
            Get all the Info regarding your favorite Crypto
          </StyledBannerContent>
        </StyledTagline>
        <CarouselView></CarouselView>
      </StyledContainerBanner>
    </StyledWrapperBanner>
  );
};

export default Banner;
