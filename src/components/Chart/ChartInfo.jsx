import React, { useEffect, useState } from 'react';

import axios from 'axios';
import { Line } from 'react-chartjs-2';
import Chart from 'chart.js/auto';
import { useSelector } from 'react-redux';
import { historicalChart, chartDays } from '../../config';
import { useParams } from 'react-router-dom';
import {
  StyledChartInfoContainer,
  StyledSelectButtonContainer,
  StyledButton
} from './Chart.styles';

const ChartInfo = () => {
  const { id } = useParams();

  const currency = useSelector((state) => state.currency);

  const [historicData, setHistoricData] = useState();
  const [days, setDays] = useState(1);
  const [flag, setflag] = useState(false);

  const fetchHistoricData = async () => {
    const { data } = await axios.get(historicalChart(id, days, currency.currency));
    setflag(true);
    setHistoricData(data.prices);
  };

  useEffect(() => {
    fetchHistoricData();
  }, [days, currency.currency]);

  return (
    <StyledChartInfoContainer>
      {' '}
      {!historicData ? null : (
        <>
          <Line
            data={{
              labels: historicData.map((coin) => {
                let date = new Date(coin[0]);
                let time =
                  date.getHours() > 12
                    ? `${date.getHours() - 12}:${date.getMinutes()} PM`
                    : `${date.getHours()}:${date.getMinutes()} AM`;
                return days === 1 ? time : date.toLocaleDateString();
              }),

              datasets: [
                {
                  data: historicData.map((coin) => coin[1]),
                  label: `Price ( Past ${days} Days ) in ${currency.currency}`,
                  borderColor: '#EEBC1D'
                }
              ]
            }}
            options={{
              elements: {
                point: {
                  radius: 1
                }
              }
            }}
          />
          <StyledSelectButtonContainer>
            {chartDays.map((day) => (
              <StyledButton
                key={day.value}
                onClick={() => {
                  setDays(day.value);
                  setflag(false);
                }}
                value={day.value === days}
              >
                {day.label}
              </StyledButton>
            ))}
          </StyledSelectButtonContainer>
        </>
      )}
    </StyledChartInfoContainer>
  );
};

export default ChartInfo;
