import styled from 'styled-components';
export const StyledChartDashboard = styled.div`
  background-color: #14161a;
  color: white;
  min-height: 100vh;
`;
export const StyledChartContainer = styled.div`
  display: flex;
  @media (max-width: 960px) {
    flex-direction: column;
    align-items: center;
  }
`;
export const StyledChartSidebar = styled.div`
  width: 30%;
  @media (max-width: 960px) {
    width: 100%;
  }

  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 100px;
  border-right: 2px solid grey;
`;
export const StyledChartHeading = styled.h3`
  font-weight: bold;
  margin-bottom: 20px;
`;

export const StyledChartDescription = styled.span`
  width: 100%;
  padding: 25px;
  padding-bottom: 15px;
  padding-top: 0px;
  text-align: justify;
`;
export const StyledChartMarketData = styled.div`
  align-self: start;
  padding: 25;
  padding-top: 10;
  width: 100%;
  @media (min-width: 960px) {
    display: flex;
    justify-content: space-around;
  }
  @media (min-width: 600px) {
    flex-direction: column;
    align-items: center;
  }
  @media (min-width: 0px) {
    align-items: start;
  }
`;
export const StyledChartImg = styled.img`
  src: ${(props) => props.image};
  alt: ${(props) => props.name};
  height: 200px;
  margin-bottom: 20px;
`;
export const StyledChartSpan = styled.span``;
export const StyledChartInfoContainer = styled.div`
  display: flex;
  width: 75%;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 40px;
  margin-top: 40px;
  @media (max-width: 960px) {
    width: 100%;
    padding: 20px;
    margin-top: 0px;
    padding-top: 0px;
  }
`;
export const StyledSelectButtonContainer = styled.div`
  display: flex;
  margin-top: 20px;
  justify-content: space-around;
  width: 100%;
`;
export const StyledButton = styled.button``;
