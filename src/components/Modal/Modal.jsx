import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideModal } from '../../redux';

import {
  StyledModal,
  StyledModalOverlay,
  StyledCloseButton,
  IconDoneStyleWrapper,
  StyledModalTitle
} from './Modal.styles';

const Modal = () => {
  const modal = useSelector((state) => state.modal);
  const dispatch = useDispatch();
  const handleModal = () => dispatch(hideModal());

  const onCloseButtonClick = () => {
    handleModal();
  };

  if (!modal.isVisible) {
    return null;
  }

  return (
    <StyledModalOverlay>
      <StyledModal>
        <StyledCloseButton onClick={onCloseButtonClick}>&#10005;</StyledCloseButton>
        <IconDoneStyleWrapper />
        <StyledModalTitle> {modal.title.title}</StyledModalTitle>
      </StyledModal>
    </StyledModalOverlay>
  );
};

export default Modal;
