import styled from 'styled-components';

export const TableContainer = styled.div`
  text-align: center;
`;

export const TableHeader = styled.div``;
export const TableInputField = styled.input`
  width: 70%;
  margin: 10px;
  padding: 7px;
  border: 3px solid #eebc1d;
`;

export const StyledTable = styled.table`
  width: 70%;
  margin-left: 15%;
  margin-right: 15%;
`;

export const THead = styled.thead`
  background-color: #eebc1d;
`;

export const TBody = styled.tbody``;

export const TR = styled.tr`
  cursor: pointer;
`;

export const TH = styled.th`
  color: black;
  font-weight: 700;
`;

export const TD = styled.td``;

export const STImage = styled.img`
  margin-bottom: 10;
  height: 50px;
`;
export const STDiv = styled.div`
  display: flex;
  flex-direction: column;
`;
export const STSpan = styled.span`
  color: darkgrey;
`;

export const STPagination = styled.a`
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
`;
