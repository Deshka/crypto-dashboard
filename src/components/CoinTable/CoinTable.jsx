import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { coinList } from '../../config';
import { numberWithCommas } from '../../utils';
import { Pagination } from '@material-ui/lab';
import {
  STDiv,
  STImage,
  STSpan,
  StyledTable,
  TableContainer,
  TableHeader,
  TableInputField,
  TBody,
  TD,
  TH,
  THead,
  TR
} from './CoinTable.styles';

const CoinTable = () => {
  const currency = useSelector((state) => state.currency);

  const [coins, setCoins] = useState([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');
  const [page, setPage] = useState(1);

  const fetchCoins = async () => {
    setLoading(true);
    const { data } = await axios.get(coinList(currency.currency));

    setCoins(data);
    setLoading(false);
  };

  const navigate = useNavigate();

  const handleSearch = () => {
    return coins.filter(
      (coin) =>
        coin.name.toLowerCase().includes(search) ||
        coin.symbol.toLowerCase().includes(search)
    );
  };

  useEffect(() => {
    fetchCoins();
  }, [currency]);
  return (
    <TableContainer>
      <TableHeader> Cryptocurrency Prices by Market Cap</TableHeader>
      <TableInputField
        placeholder="Search For a Crypto Currency.."
        onChange={(e) => setSearch(e.target.value)}
      />

      {loading ? null : (
        <StyledTable>
          <THead>
            <TR>
              {['Coin', 'Price', '24h Change', 'Market Cap'].map((head) => (
                <TH key={head}>{head}</TH>
              ))}
            </TR>
          </THead>
          <TBody>
            {handleSearch()
              .slice((page - 1) * 10, (page - 1) * 10 + 10)
              .map((row) => {
                const profit = row.price_change_percentage_24h > 0;
                return (
                  <TR onClick={() => navigate(`/coins/${row.id}`)} key={row.name}>
                    <TH
                      style={{
                        display: 'flex',
                        gap: 15
                      }}
                    >
                      <STImage src={row?.image} alt={row.name} />
                      <STDiv>
                        <STSpan
                          style={{
                            textTransform: 'uppercase',
                            fontSize: 22
                          }}
                        >
                          {row.symbol}
                        </STSpan>
                        <STSpan>{row.name}</STSpan>
                      </STDiv>
                    </TH>
                    <TD align="right">
                      {currency.symbol}{' '}
                      {numberWithCommas(row.current_price.toFixed(2))}
                    </TD>
                    <TD
                      align="right"
                      style={{
                        color: profit > 0 ? 'rgb(14, 203, 129)' : 'red',
                        fontWeight: 500
                      }}
                    >
                      {profit && '+'}
                      {row.price_change_percentage_24h.toFixed(2)}%
                    </TD>
                    <TD align="right">
                      {currency.symbol}{' '}
                      {numberWithCommas(row.market_cap.toString().slice(0, -6))}M
                    </TD>
                  </TR>
                );
              })}
          </TBody>
        </StyledTable>
      )}

      <Pagination
        count={handleSearch()?.length / 10}
        style={{
          padding: 20,
          width: '100%',
          display: 'flex',
          justifyContent: 'center'
        }}
        onChange={(_, value) => {
          setPage(value);
        }}
      />
    </TableContainer>
  );
};

export default CoinTable;
