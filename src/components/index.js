import ProtectedRoute from './ProtectedRoute';
import {
  SFlexContainer,
  SFlexDiv,
  FlexWrapper
} from './Form/Container/Container.styles';
import Modal from './Modal/Modal';
import Banner from './Banner/Banner';
import ChartInfo from './Chart/ChartInfo';
import {
  SForm,
  IconStyleWrapper,
  StyledTitle,
  StyledInput,
  StyledError,
  SmallStyledTitle,
  StyledButton,
  SRedirect,
  SRedirectLabel,
  SRedirectLink
} from './Form/Form/Form.styles';
import Navbar from './Navbar/Navbar';

import {
  Wrapper,
  Card,
  CardHeader,
  Avatar,
  Title,
  NameInput,
  NameParagraph,
  StyledParagraph,
  Stats,
  EditStyledIcon,
  SaveStyledSave
} from './Card/Card.styles';
import { StyledDashboardContainer } from './Banner/Banner.styles';
import CarouselView from './Banner/Carousel';
import {
  StyledChartDashboard,
  StyledChartContainer,
  StyledChartSidebar,
  StyledChartHeading,
  StyledChartDescription,
  StyledChartMarketData,
  StyledChartImg,
  StyledChartSpan
} from './Chart/Chart.styles.jsx';
import CoinTable from './CoinTable/CoinTable';
export {
  ProtectedRoute,
  SForm,
  IconStyleWrapper,
  StyledTitle,
  StyledInput,
  StyledError,
  StyledButton,
  SmallStyledTitle,
  SFlexContainer,
  SRedirect,
  SRedirectLabel,
  SRedirectLink,
  Modal,
  Navbar,
  Wrapper,
  Card,
  CardHeader,
  Avatar,
  Title,
  NameInput,
  NameParagraph,
  StyledParagraph,
  Stats,
  EditStyledIcon,
  SaveStyledSave,
  SFlexDiv,
  FlexWrapper,
  Banner,
  StyledDashboardContainer,
  CarouselView,
  ChartInfo,
  StyledChartDashboard,
  StyledChartContainer,
  StyledChartSidebar,
  StyledChartHeading,
  StyledChartDescription,
  StyledChartMarketData,
  StyledChartImg,
  StyledChartSpan,
  CoinTable
};
