import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateUser } from '../redux';
import {
  Wrapper,
  Card,
  CardHeader,
  Avatar,
  Title,
  NameInput,
  StyledParagraph,
  Stats,
  EditStyledIcon,
  SaveStyledSave,
  NameParagraph,
  Navbar
} from '../components';

const Profile = () => {
  const userData = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const handleUpdateUser = (values) => dispatch(updateUser(values));

  const [firstName, setFirstName] = useState(userData.user.user.firstName);
  const [lastName, setLastName] = useState(userData.user.user.lastName);

  const [edit, setEdit] = useState(false);

  const changeFirstName = (e) => {
    setFirstName(e.target.value);
  };

  const lastFirstName = (e) => {
    setLastName(e.target.value);
  };

  const handleUpdate = () => {
    handleUpdateUser({
      id: userData.user.user.id,
      firstName: firstName,
      lastName: lastName
    });
    setEdit(false);
  };

  return (
    <div>
      <Navbar />
      <Wrapper>
        <Card>
          <CardHeader>
            <Avatar src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSUZufYn_dxi91143X8E3p0QLJQRJ2XxvJzjbpJm1eNnPkrBzInQ5jKWNXyIchAnIv4kao&usqp=CAU" />

            {edit ? (
              <Title>
                <NameInput
                  type="text"
                  value={firstName}
                  onChange={changeFirstName}
                />
                <NameInput value={lastName} onChange={lastFirstName} />{' '}
              </Title>
            ) : (
              <Title>
                <NameParagraph>{firstName}</NameParagraph>
                <NameParagraph>{lastName}</NameParagraph>
              </Title>
            )}

            <StyledParagraph>{userData.user.user.email}</StyledParagraph>
          </CardHeader>
          <Stats>
            {!edit ? (
              <EditStyledIcon onClick={() => setEdit(true)} />
            ) : (
              <SaveStyledSave onClick={handleUpdate} />
            )}
          </Stats>
        </Card>
      </Wrapper>
    </div>
  );
};

export default Profile;
