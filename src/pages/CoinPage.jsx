import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import {
  ChartInfo,
  StyledChartDashboard,
  StyledChartContainer,
  StyledChartSidebar,
  StyledChartHeading,
  StyledChartDescription,
  StyledChartMarketData,
  StyledChartImg,
  StyledChartSpan,
  Navbar
} from '../components';
import { singleCoin } from '../config';

const CoinPage = () => {
  const { id } = useParams();
  const currency = useSelector((state) => state.currency);
  const [coin, setCoin] = useState();

  const fetchCoin = async () => {
    const { data } = await axios.get(singleCoin(id));

    setCoin(data);
  };

  useEffect(() => {
    fetchCoin();
  }, []);

  return (
    <StyledChartDashboard>
      <Navbar />
      <StyledChartContainer>
        <StyledChartSidebar>
          <StyledChartImg src={coin?.image.large} alt={coin?.name} />

          <StyledChartHeading>{coin?.name}</StyledChartHeading>
          <StyledChartDescription>
            {ReactHtmlParser(coin?.description.en.split('. ')[0])}.
          </StyledChartDescription>
          <StyledChartMarketData>
            <StyledChartSpan style={{ display: 'flex' }}>
              <StyledChartHeading>Rank:</StyledChartHeading>
              &nbsp; &nbsp;
              <StyledChartSpan>{coin?.market_cap_rank}</StyledChartSpan>
            </StyledChartSpan>
            <StyledChartSpan style={{ display: 'flex' }}>
              <StyledChartHeading> Current Price:</StyledChartHeading>
              &nbsp; &nbsp;
              <StyledChartSpan>
                {currency.symbol}{' '}
                {coin?.market_data.current_price[currency.currency.toLowerCase()]}
              </StyledChartSpan>
            </StyledChartSpan>
            <StyledChartSpan style={{ display: 'flex' }}>
              <StyledChartHeading> Market Cap:</StyledChartHeading>
              &nbsp; &nbsp;
              <StyledChartSpan>
                {currency.symbol}{' '}
                {coin?.market_data.market_cap[currency.currency.toLowerCase()]
                  .toString()
                  .slice(0, -6)}{' '}
                M
              </StyledChartSpan>
            </StyledChartSpan>
          </StyledChartMarketData>
        </StyledChartSidebar>
        <ChartInfo />
      </StyledChartContainer>
    </StyledChartDashboard>
  );
};

export default CoinPage;
