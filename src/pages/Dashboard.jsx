import React from 'react';

import { Banner, CoinTable, Navbar, StyledChartDashboard } from '../components';

const Dashboard = () => {
  return (
    <StyledChartDashboard>
      <Navbar />
      <Banner />
      <CoinTable />
    </StyledChartDashboard>
  );
};
export default Dashboard;
