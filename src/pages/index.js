import Login from './Login';
import NotFound from './NotFound';
import Register from './Register';
import Profile from './Profile';
import Dashboard from './Dashboard';
import CoinPage from './CoinPage';
export { Login, NotFound, Register, Profile, Dashboard, CoinPage };
